package logginglibrary

import (
	"article/integer_const/log2"
	"log"
)

func getLevelString(lvl int) string {
	switch lvl {
	case log2.OFF:
		return "OFF"
	case log2.FATAL:
		return "FATAL"
	case log2.ERROR:
		return "ERROR"
	case log2.WARN:
		return "WARN"
	case log2.INFO:
		return "INFO"
	case log2.DEBUG:
		return "DEBUG"
	case log2.TRACE:
		return "TRACE"
	case log2.ALL:
		return "ALL"
	}

	return ""
}

func Log(lvl int, message string) {
	log.Printf("%v [%v] %v", getLevelString(lvl), lvl, message)
}
