package log2

import "math"

const (
	OFF   = 0
	FATAL = 100
	ERROR = 200
	WARN  = 300
	INFO  = 400
	DEBUG = 500
	TRACE = 600
	ALL   = math.MaxInt64
)
