package main

import (
	"article/integer_const/log2"
	logginglibrary "article/integer_const/loggingLibrary"
)

// _________________________ Logging library package _________________________

func main() {
	logginglibrary.Log(log2.WARN, "my msg")

	// classic levels can not be modified
	// custom levels can not be added
}
