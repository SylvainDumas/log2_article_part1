package logginglibrary

import (
	"article/string_const/log2"
	"log"
	"math"
	"sync"
)

var levels sync.Map

func init() {
	levels.Store(log2.OFF, 0)
	levels.Store(log2.FATAL, 100)
	levels.Store(log2.ERROR, 200)
	levels.Store(log2.WARN, 300)
	levels.Store(log2.INFO, 400)
	levels.Store(log2.DEBUG, 500)
	levels.Store(log2.TRACE, 600)
	levels.Store(log2.ALL, int(math.MaxInt64))
}

func SetLevel(lvl string, value int) {
	levels.Store(lvl, value)
}

func getLevelValue(lvl string) int {
	if value, found := levels.Load(lvl); found {
		return value.(int)
	}
	return 0
}

func Log(lvl string, message string) {
	log.Printf("%v [%v] %v", lvl, getLevelValue(lvl), message)
}
