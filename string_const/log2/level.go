package log2

const (
	OFF   = "OFF"
	FATAL = "FATAL"
	ERROR = "ERROR"
	WARN  = "WARN"
	INFO  = "INFO"
	DEBUG = "DEBUG"
	TRACE = "TRACE"
	ALL   = "ALL"
)
