package main

import (
	"article/string_const/log2"
	logginglibrary "article/string_const/loggingLibrary"
)

// _________________________ Application _________________________

func main() {
	logginglibrary.Log(log2.WARN, "my msg")

	// Redefine existing
	logginglibrary.SetLevel("WARN", 3)
	logginglibrary.Log(log2.WARN, "my warn msg with alias")
	// or
	logginglibrary.Log("WARN", "my warn msg with plain string")

	// Define new custom
	logginglibrary.SetLevel("CUSTOM", 42)
	logginglibrary.Log("CUSTOM", "my msg for custom level")

	// Check for same value for two different levels
	logginglibrary.SetLevel("WARN", 300)
	var FOO = "FOO"
	logginglibrary.SetLevel(FOO, 300)
	logginglibrary.Log(FOO, "my msg for foo level")
	logginglibrary.Log(log2.WARN, "my msg for warn level")
	logginglibrary.Log("FOO", "my msg for foo level")
}
