package logginglibrary

import (
	"article/integer_atomic/log2"
	"log"
	"sync"
)

var levels sync.Map

func SetLevel(lvlString string, lvl int32) {
	switch lvlString {
	case "OFF":
		log2.SetOFF(lvl)
	case "FATAL":
		log2.SetFATAL(lvl)
	case "ERROR":
		log2.SetERROR(lvl)
	case "WARN":
		log2.SetWARN(lvl)
	case "INFO":
		log2.SetINFO(lvl)
	case "DEBUG":
		log2.SetDEBUG(lvl)
	case "TRACE":
		log2.SetTRACE(lvl)
	case "ALL":
		log2.SetALL(lvl)
	default:
		// we must have for one string, one level value
		// if level is used as key, if we set another string for same value, we loose previous string
		levels.Store(lvlString, lvl)
	}
}

func getLevelString(lvl int32) string {
	switch lvl {
	case log2.OFF():
		return "OFF"
	case log2.FATAL():
		return "FATAL"
	case log2.ERROR():
		return "ERROR"
	case log2.WARN():
		return "WARN"
	case log2.INFO():
		return "INFO"
	case log2.DEBUG():
		return "DEBUG"
	case log2.TRACE():
		return "TRACE"
	case log2.ALL():
		return "ALL"
	}

	var back string
	levels.Range(func(key interface{}, value interface{}) bool {
		if value.(int32) != lvl {
			return true
		}
		back = key.(string)
		return false
	})

	return back
}

func Log(lvl int32, message string) {
	log.Printf("%v [%v] %v", getLevelString(lvl), lvl, message)
}
