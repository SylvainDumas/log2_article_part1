package main

import (
	"article/integer_atomic/log2"
	logginglibrary "article/integer_atomic/loggingLibrary"
)

// _________________________ Application package _________________________

func main() {
	logginglibrary.Log(log2.WARN(), "my msg")

	// Redefine existing
	logginglibrary.SetLevel("WARN", 3)
	logginglibrary.Log(log2.WARN(), "my warn msg with alias function")
	// or
	logginglibrary.Log(3, "my warn msg with level value")

	// Define new custom
	logginglibrary.SetLevel("CUSTOM", 42)
	logginglibrary.Log(42, "my msg for custom level")

	// Check for same value for two different levels
	logginglibrary.SetLevel("WARN", 300)
	var FOO int32 = 300
	logginglibrary.SetLevel("FOO", FOO)
	logginglibrary.Log(FOO, "my msg for foo level")          // Keep in mind that the value for WARN is 300 and for FOO is 300.
	logginglibrary.Log(log2.WARN(), "my msg for warn level") // Keep in mind that the value for WARN is 300 and for FOO is 300.
	logginglibrary.Log(300, "my msg for 300 ?? level")       // Keep in mind that the value for WARN is 300 and for FOO is 300.
}
