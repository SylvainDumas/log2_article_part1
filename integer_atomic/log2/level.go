package log2

import (
	"math"
	"sync/atomic"
)

var (
	off   int32 = 0
	fatal int32 = 100
	error int32 = 200
	warn  int32 = 300
	info  int32 = 400
	debug int32 = 500
	trace int32 = 600
	all   int32 = math.MaxInt32
)

func OFF() int32 {
	return atomic.LoadInt32(&off)
}

func SetOFF(value int32) {
	atomic.StoreInt32(&off, value)
}

func FATAL() int32 {
	return atomic.LoadInt32(&fatal)
}

func SetFATAL(value int32) {
	atomic.StoreInt32(&fatal, value)
}

func ERROR() int32 {
	return atomic.LoadInt32(&error)
}

func SetERROR(value int32) {
	atomic.StoreInt32(&error, value)
}

func WARN() int32 {
	return atomic.LoadInt32(&warn)
}

func SetWARN(value int32) {
	atomic.StoreInt32(&warn, value)
}

func INFO() int32 {
	return atomic.LoadInt32(&info)
}

func SetINFO(value int32) {
	atomic.StoreInt32(&info, value)
}

func DEBUG() int32 {
	return atomic.LoadInt32(&debug)
}

func SetDEBUG(value int32) {
	atomic.StoreInt32(&debug, value)
}

func TRACE() int32 {
	return atomic.LoadInt32(&trace)
}

func SetTRACE(value int32) {
	atomic.StoreInt32(&trace, value)
}

func ALL() int32 {
	return atomic.LoadInt32(&all)
}

func SetALL(value int32) {
	atomic.StoreInt32(&all, value)
}
